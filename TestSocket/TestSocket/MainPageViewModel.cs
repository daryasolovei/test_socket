﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace TestSocket
{
    public class MainPageViewModel : BaseModel
    {
        public string SocketUri { get; set; } = "ws://my.xltracking.net:4447/";
        public string Message { get; set; }
        public string SocketState { get; set; }
        public int TimerInt { get; set; }
        public string TimerString => $"{TimerInt}";

        ClientWebSocket client;

        public System.Timers.Timer timer;

        public MainPageViewModel() { }

        public ICommand SocketConnectCommand => new Command(async () => await SocketConnectAsync());
        public ICommand SendMessageCommand => new Command(async () => await SendMessageAsync());

        async Task SocketConnectAsync()
        {
            client = new ClientWebSocket();

            timer = new System.Timers.Timer
            {
                Interval = 1000
            };
            TimerInt = 0;

            timer.Elapsed += (s, e) =>
            {
                TimerInt++;
            };

            using (UserDialogs.Instance.Loading())
            {
                try
                {
                    timer.Start();

                    await client.ConnectAsync(new Uri(SocketUri), CancellationToken.None);

                    SocketState = client.State.ToString();
                }
                catch (Exception exception)
                {
                    SocketState = client.State.ToString();
                    UserDialogs.Instance.Alert(exception.Message);
                }

                timer.Stop();
            }
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            throw new NotImplementedException();
        }

        async Task SendMessageAsync()
        {
            var msg = new MessageModel
            {
                Text = Message
            };

            string serialisedMessage = JsonConvert.SerializeObject(msg);

            var byteMessage = Encoding.UTF8.GetBytes(serialisedMessage);
            var segmnet = new ArraySegment<byte>(byteMessage);

            await client.SendAsync(segmnet, WebSocketMessageType.Text, true, CancellationToken.None);
        }
    }
}
